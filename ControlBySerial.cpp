#include "ControlBySerial.h"

ControlBySerial::ControlBySerial(RTC_DS3231 &_rtc, byte _addressEEPROM, const char *_nameProgram, const char *_versionProgram)
{
	rtc_lib = _rtc;
	addressEEPROM = _addressEEPROM; 
	*nameProgram = *_nameProgram;
	*versionProgram = *_versionProgram;
}

void ControlBySerial::readMessage()	// GOOD
{
	const char symbol = Serial.read();	// прочитали символ
	
	switch (waiting_symbol)
	{
		case WAITING_START: // Ждём symbolOfStart и игнорируем любые другие
			if (symbol != symbolOfStart) return;
			waiting_symbol = WAITING_END; // Пришёл symbolOfStart. Переходим в состояние WAITING_END
			break;
		case WAITING_END: // ждём symbolOfEnd. Любые другие складываем в буфер.
			if (symbol != symbolOfEnd)
			{
				message = message + symbol;
				if (message.length()+1 > 40)
				{
					sendMessageToComp(textCommandTooLong);
					waiting_symbol = WAITING_START;
					message = "";
				}
			} else
			{
				parseString(message);
				waiting_symbol = WAITING_START;
				message = "";
			}
			break;
	}		
}

void ControlBySerial::parseString(String buffer)	// GOOD
{
	static byte mountOfValue = 0;
	
	switch (parse_state)
	{
		case WAITING_COMMAND:
			mountOfValue = checkCommandAndExecute(buffer);
			break;
		case WAITING_VALUE:
			if(checkValue(buffer, mountOfValue))
			{
				applyCommand(command);
			}
			break;
	}
	Serial.println("All Done!");	// удалить позже
}

byte ControlBySerial::checkCommandAndExecute(String buffer)	// 
{
	parse_state = WAITING_COMMAND;
	if (buffer.equals("help"))
	{
		helpShow();
		// parse_state = WAITING_COMMAND;
		return 0;
	}
	
	if (buffer.equals("timeShow"))
	{
		timeShow();
		// parse_state = WAITING_COMMAND;
		return 0;
	}
	
	if (buffer.equals("timeSet"))
	{
		timeSetExampleSet();
		parse_state = WAITING_VALUE;
		command = TIME_SET;
		return 6;
	}
	
	if (buffer.equals("colorShow"))
	{
		colorShow();
		// parse_state = WAITING_COMMAND;
		return 0;
	}
	
	if (buffer.equals("colorSet"))
	{
		colorSetExampleShow();
		parse_state = WAITING_VALUE;
		command = COLOR_SET;
		return 4;
	}
	
	if (buffer.equals("lightOnOffShow"))
	{
		lightOnOffShow();
		// parse_state = WAITING_COMMAND;
		return 0;
	}
	
	if (buffer.equals("lightOnOffSet"))
	{
		lightOnOffExampleShow();
		parse_state = WAITING_VALUE;
		command = LIGHT_ON_OFF_SET;
		return 4;
	}
	
	if (buffer.equals("durationOnOffShow"))
	{
		durationOnOffShow();
		// parse_state = WAITING_COMMAND;
		return 0;
	}
	
	if (buffer.equals("durationOnOffSet"))
	{
		durationOnOffSetExampleShow();
		parse_state = WAITING_VALUE;
		command = DURATION_ON_OFF_SET;
		return 2;
	}

	if (buffer.equals("cancel"))
	{
		// Serial.print("cancel - ");
		cancelInputValue();
		// parse_state = WAITING_COMMAND;
		return 0;
	}
		
	commandNotFound();
	
	return COMMAND_NOT_FOUND;
		
}

byte ControlBySerial::applyCommand(byte _command)	// GOOD
{
	switch (_command) {
		case TIME_SET:
			timeSet();
			Serial.println("timeSet");
			break;
		case COLOR_SET:
			colorSet();
			Serial.println("colorSet");
			break;
		case LIGHT_ON_OFF_SET:
			lightOnOffSet();
			Serial.println("lightOnOffSet");
			break;
		case DURATION_ON_OFF_SET:
			durationOnOffSet();
			Serial.println("durationOnOffSet");
			break;
		case COMMAND_NOT_FOUND:
			commandNotFound();
			break;
		default:
			return WAITING_COMMAND;
    }
}

void ControlBySerial::helpShow()	// GOOD
{
	sendMessageToComp(lineSeparator);	// *******************
	sendMessageToComp(textTimeShow);
	sendMessageToComp(textColorShow);
	sendMessageToComp(textLightOnOffShow);
	sendMessageToComp(textDurationOnOffShow);
	sendMessageToComp(lineSeparator);	// *******************
}

void ControlBySerial::timeShow()	// GOOD
{
	now_lib = rtc_lib.now();
	printDigits(now_lib.hour());
    Serial.print(':');
	printDigits(now_lib.minute());
    Serial.print(':');
	printDigits(now_lib.second());
	Serial.print(" ");
	printDigits(now_lib.year());
    Serial.print('/');
	printDigits(now_lib.month());
    Serial.print('/');
	printDigits(now_lib.day());
    Serial.println();
}

void ControlBySerial::timeSetExampleSet()	// GOOD
{
	sendMessageToComp(textTimeSetExample);
}

void ControlBySerial::timeSet()	// NOT GOOD
{
	// rtc_lib.adjust(DateTime(F(__DATE__), F(__TIME__)));
	if(timeValueValidation())
	{
		rtc_lib.adjust(DateTime(value[0], value[1], value[2], value[3], value[4], value[5]));
		timeShow();
		parse_state = WAITING_COMMAND;
		return;
	}
	
	cleanAllValue();
	sendMessageToComp(textValuesNotCorrect);
	parse_state = WAITING_VALUE;
}

byte ControlBySerial::timeValueValidation()	// GOOD
{	// $YYYY@MM@DD@HH@MM@SS@#
	if(value[0] > 2030) return 0; 	// YYYY
	if(value[1] > 12) return 0;		// MM
	if(value[2] > 31) return 0; 	// DD
	if(value[3] > 31) return 0;		// HH
	if(value[4] > 60) return 0;		// MM
	if(value[5] > 60) return 0;		// SS
	
	return 1;
}

void ControlBySerial::cleanAllValue()	// GOOD
{
	for(byte count = 0; count < 6; count++)
	{
		value[count] = 0;
	}
}

void ControlBySerial::colorShow()	// GOOD
{
	byte colorPWM = 0;
	
	for (byte count = 0; count < 4; count++)
	{
		colorPWM = EEPROM.read(addressEEPROM + count);
		Serial.print(colorList[count]);
		Serial.println(colorPWM);
	}
}

void ControlBySerial::colorSetExampleShow()	// GOOD
{
	sendMessageToComp(textColorSetExample);
	// parse_state = WAITING_VALUE;
}

void ControlBySerial::colorSet()	//
{
	for(byte count = 0; count < 5; count++)	// проверяем корректность значений
	{
		if(value[count] > 255)
		{
			sendMessageToComp(textValuesNotCorrect);
			return;
		}
	}
	
	for(byte count = 0; count < 5; count++)
	{
		// EEPROM.write(1, value[count]);
		writeToEEPROM(addressEEPROM + count, value[count]);
	}
	
	
	parse_state = WAITING_COMMAND;
}

void ControlBySerial::lightOnOffShow()	//
{
	byte hour = 0;
	byte minute = 0;
	hour = EEPROM.read(addressEEPROM + 6);
	minute = EEPROM.read(addressEEPROM + 7);
	Serial.print("Light on - ");
	printDigits(hour);
	Serial.print(":");
	printDigits(minute);
	Serial.println("");
	
	hour = EEPROM.read(addressEEPROM + 8);
	minute = EEPROM.read(addressEEPROM + 9);
	Serial.print("Light off - ");
	printDigits(hour);
	Serial.print(":");
	printDigits(minute);
	Serial.println("");
}


void ControlBySerial::lightOnOffExampleShow()	// GOOD
{
	sendMessageToComp(textLightOnOffSetExample);
	// parse_state = WAITING_VALUE;
}

void ControlBySerial::lightOnOffSet()	//
{
	
	parse_state = WAITING_COMMAND;
}

void ControlBySerial::durationOnOffShow()	//
{
	byte duration = 0;
	
	duration = EEPROM.read(addressEEPROM + 4);
	Serial.print("Duration light on - ");
	Serial.println(duration);
	duration = EEPROM.read(addressEEPROM + 5);
	Serial.print("Duration light off - ");
	Serial.println(duration);
}

void ControlBySerial::durationOnOffSetExampleShow()	// GOOD
{
	sendMessageToComp(textDurationOnOffSetExample);
	// parse_state = WAITING_VALUE;
}

void ControlBySerial::durationOnOffSet()
{
	
	parse_state = WAITING_COMMAND;
}

void ControlBySerial::commandNotFound()	// OK
{
	sendMessageToComp(textCommandNotFound);
}

void ControlBySerial::cancelInputValue()
{
	parse_state = WAITING_COMMAND;
}

void ControlBySerial::printDigits(int digits)	// GOOD
{
	if(digits < 10)
	{
		Serial.print("0");
	}
	Serial.print(digits);
}

void ControlBySerial::sendMessageToComp(const char *_text)	// GOOD
{
	char convertText;
	byte len = strlen_P(_text);
	for (byte i = 0; i < len; i++)
	{
		convertText =  pgm_read_byte_near(_text + i);
		Serial.print(convertText);
	}
	Serial.println("");
}

byte ControlBySerial::checkValue(String buffer, byte mountOfValue)	// 
{	// $YYYY@MM@DD@HH@MM@SS@#
	int pos_start = 0;
	int pos_end = 1;
	byte count = 0;
	
	cleanAllValue();
	
	while (pos_end < buffer.length()-1)
	{
		pos_end = buffer.indexOf('@', pos_start); // ищем '@' - окончание значения	
		value[count] = buffer.substring(pos_start, pos_end).toInt(); // берем значение
			// Serial.print("pos_start - ");
			// Serial.println(pos_start);
			// Serial.print("pos_end - ");
			// Serial.println(pos_end);
			Serial.print("value[] - ");
			Serial.println(value[count]);
		pos_start = pos_end + 1;	// смещаем позиуию начала команды
		count++; // перенести  выше
	}
	
	if (count == mountOfValue)
	{
		Serial.println("ok");
		return 1;
	} else
	{
		for(byte i = 0; i <= count; i++)	// ???
		{
			value[count] = 0;
		}
		Serial.println("not ok");
		return 0;
	}
	parse_state = WAITING_COMMAND;
}

void ControlBySerial::writeToEEPROM(byte address, byte value)	// 
{
	if (value != EEPROM.read(address))
	{
		EEPROM.write(address, value);
	}
}