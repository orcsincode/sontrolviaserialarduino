/*
  ControlBySerial.h - Library for control Arduino by Serial port.
  Created by Krikun K. January 5, 2020.
  Released into the public domain.
*/
#ifndef ControlBySerial_h
#define ControlBySerial_h

#include "Arduino.h"
#include <avr/pgmspace.h>
#include <EEPROM.h>
#include <RTClib.h>

const char textTimeShow[ ] PROGMEM = "Enter $timeShow# or $timeSet#";
const char textTimeSetExample[ ] PROGMEM = "Enter time now $YYYY@MM@DD@HH@MM@SS@#";	// 6

const char textColorShow[ ] PROGMEM = "Enter $colorShow# or $colorSet#";
const char textColorSetExample[ ] PROGMEM = "Enter (0-255) $Blue@Red@WhiteWarm@WhiteCold@#";	// 4

const char textLightOnOffShow[ ] PROGMEM = "Enter $lightOnOffShow# or $lightOnOffSet#";
const char textLightOnOffSetExample[ ] PROGMEM = "Enter On and Off time $HH@MM@HH@MM@#";	// 4

const char textDurationOnOffShow[ ] PROGMEM = "Enter $durationOnOffShow# or $durationOnOffSet#";
const char textDurationOnOffSetExample[ ] PROGMEM = "Enter (0-255)minute $DurationOn@DurationOff@#";	// 2

const char textCommandNotFound[ ] PROGMEM = "Command not found! Enter @help#";
const char textCommandTooLong[ ] PROGMEM = "ERROR! Command too long!";
const char textValuesNotCorrect[ ] PROGMEM = "Values not correct! Repeat please or input $cancel#.";

const char lineSeparator[ ] PROGMEM = "******************";



class ControlBySerial
{
  public:
	// variable
	
	// function
    ControlBySerial(RTC_DS3231 &rtc, byte addressEEPROM, const char *nameProgram, const char *versionProgram);
	void readMessage();
	void helpShow();
	
  private:
	// variable
	RTC_DS3231 rtc_lib;
	DateTime now_lib;
	
	byte addressEEPROM;
	char *nameProgram;
	char *versionProgram;
	const char symbolOfStart = '$';
	const char symbolOfSeparation = '@';
	const char symbolOfEnd = '#';
	
	const String colorList[4] = {"Blue - ", "Red - ", "White Warm - ", "White Cold - "};
	
	long value[6] ={0};
	
	byte numberOfVariables = 0;
	
	String message;
	
	enum WAITING_SYMBOL:byte
	{
		WAITING_START,
		WAITING_END
	} waiting_symbol = WAITING_START;
	
	enum PARSE_STATE:byte
	{
		WAITING_COMMAND,
		WAITING_VALUE
	} parse_state = WAITING_COMMAND;
	
	enum COMMAND:byte
	{
		TIME_SET = 0,
		COLOR_SET = 1,
		LIGHT_ON_OFF_SET = 2,
		DURATION_ON_OFF_SET = 3,
		COMMAND_NOT_FOUND = 99
	} command = COMMAND_NOT_FOUND;
	
	// function
    void sendMessageToComp(const char *text);
	void parseString(String buffer);
	byte checkCommandAndExecute(String buffer);	// позже убрать возвратное значение
	byte checkValue(String buffer, byte mountOfValue);
	
	byte applyCommand(byte command);
	
	void timeShow();
	void timeSetExampleSet();
	void timeSet();
	byte timeValueValidation();
	
	void colorShow();
	void colorSetExampleShow();
	void colorSet();
	
	void lightOnOffShow();
	void lightOnOffExampleShow();
	void lightOnOffSet();
	
	void durationOnOffShow();
	void durationOnOffSetExampleShow();
	void durationOnOffSet();
	
	void cleanAllValue();
	
	void commandNotFound();
	void cancelInputValue();
	
	void printDigits(int digits);
	
	void writeToEEPROM(byte address, byte value);
	
};
#endif
