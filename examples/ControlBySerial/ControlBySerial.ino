#include <avr/pgmspace.h>
#include "ControlBySerial.h"
#include <Wire.h>
#include "RTClib.h"
#include <EEPROM.h>

// $123@456@789#
// $timeShow@456@789#
// $help#

RTC_DS3231 rtc;
DateTime now;

const char textProgramName[ ] PROGMEM = "AquaLed200";
const char textProgramVersion[ ] PROGMEM = "0.01";

const char textWelcome[ ] PROGMEM = "AquaMiniComputer";

ControlBySerial controlArduinoUnoBySerial(rtc, 1, textProgramName, textProgramVersion);

// String message;

void setup()
{
	Serial.begin(9600);
	Serial.println("Start");
	Wire.begin(); 		// Инициализируем часы
	rtc.begin();		// 		--//--
	controlArduinoUnoBySerial.helpShow();
	
	// message ="";

}

void serialEvent()
{
	controlArduinoUnoBySerial.readMessage();
}

void loop()
{
	now = rtc.now();	// Считываем текущее время;
	// Serial.print(now.year(), DEC);
    // Serial.print('/');
    // Serial.print(now.month(), DEC);
    // Serial.print('/');
    // Serial.print(now.day(), DEC);
    // Serial.print(" ");
    // Serial.print(now.hour(), DEC);
    // Serial.print(':');
    // Serial.print(now.minute(), DEC);
    // Serial.print(':');
    // Serial.print(now.second(), DEC);
    // Serial.println();
	// delay(1000);
}
